var MyObject = {
	multiply: function (a, b) {
		return a*b;
	},

	divide: function (a, b) {
		return a/b;
	}
}

module.exports = MyObject;