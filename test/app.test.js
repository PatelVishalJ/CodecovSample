var chai = require('chai');
var app = require('../app/app')

describe('testing add', function() {
	it('2 + 3 = 5', function() {
		chai.expect(app.add(2,3)).to.equals(5);
	})

	it('-5 + 3 = -2', function() {
		chai.expect(app.add(-5,3)).to.equals(-2);
	})

	it('4 - 1 = 3', function() {
		chai.expect(app.subtract(4,1)).to.equals(3);
	})

})